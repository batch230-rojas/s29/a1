db.users.insertOne({
	firstName: "Jane",
	lastName: "Doe",
	age: 21,
	contact:{
		phone: "87654321",
		email: "janedoe@mail.com",
	},
	courses: ["CSS", "JavaScript", "Python"],
	department: "none"
})


db.users.insertMany(
	[
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact:{
				phone: "87000",
				email: "stephenhawking@mail.com",
			},
			courses: ["Python", "React", "PHP"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact:{
				phone: "470220",
				email: "neilarmstrong@mail.com",
			},
			courses: ["React", "Laravel", "MongoDB"],
			department: "none"
		}
	]
)



db.users.find(
		{
			$or: [
				{firstName: {
					$regex: 'S', $options: '$i'
					}

				},
				{lastName: {
					$regex: 'D', $options: '$i'
					}
				}
			]	
		},

		{
			firstName: 1,
			lastName: 1,
			_id: 0
		}
	);


db.users.find(
		{
			$and: [
				{department: "HR"},
				{age: {$gte:70}}
			]	
		}
	);


db.users.find(
		{
			$and: [
				{age: {$lte:30}},
				{firstName: {
					$regex: 'E', $options: '$i'
					}
				}
			]	
		}
	);



